# recipe-app-api-proxy
Recipe app API proxy application

## Usage

### Environment Variables

* `LISTEN_PORT` - Port to listen on (default: `8000`)
* `APP_HOST` - Hostname of the app to forward requests to (default: `app`)
* `APP_PORT` - Part of the app to forward requests to (default: `9000`)